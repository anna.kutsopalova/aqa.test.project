import { test, expect } from '@playwright/test';

test('scenario 1, Login', async ({ page }) => {

    await page.goto('https://the-internet.herokuapp.com/');

    await page.waitForTimeout(3_000);

    await expect(page.locator('[class="heading"]')).toHaveText('Welcome to the-internet');

    await page.locator('[href="/login"]').click();

    await page.waitForTimeout(3_000);

    await expect(page.locator('[class="example"] h2')).toHaveText('Login Page');

    await page.locator('[id="username"]').fill('tomsmith');

    await page.locator('[name="password"]').fill('SuperSecretPassword!');

    await page.locator('[type="submit"]').click();

    await expect(page.locator('[id="flash"]')).toContainText('You logged into a secure area!');
});